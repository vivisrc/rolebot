from discord.ext.commands import Cog
from discord.ext import commands


class Manage(Cog):
    def __init__(self, bot):
        self.bot = bot

    async def owner_or_manage_roles_check(ctx):
        return (
            ctx.author == ctx.bot.app_info.owner
            or ctx.author.guild_permissions.manage_roles
        )

    @commands.bot_has_permissions(manage_roles=True)
    @commands.check(owner_or_manage_roles_check)
    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.guild)
    async def clearroles(self, ctx, dryrun=False):
        """Removes unused rolebot roles in guild. (needs Manage Roles)

        This command can only be used every 60 secs per guild."""
        roleclear = await self.bot.clear_roles(ctx, dryrun)
        if roleclear is None:
            return await ctx.send(
                "I had some sort of issue when clearing "
                "roles. It was logged, devs will check "
                "it out. Sorry."
            )

        dryrun_text = "would be deleted outside dryrun" if dryrun else "deleted"

        await ctx.send(
            f"{ctx.author.mention}: Done, {roleclear} "
            f"unused rolebot role(s) {dryrun_text}."
        )

    @commands.bot_has_permissions(manage_roles=True)
    @commands.check(owner_or_manage_roles_check)
    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.guild)
    async def moveroles(self, ctx):
        """Moves rolebot roles under rolebot role. EXPERIMENTAL (needs Manage Roles)

        This command can only be used every 60 secs per guild."""
        if await self.bot.move_roles(ctx):
            await ctx.send(f"{ctx.author.mention}: Successfully moved roles.")


def setup(bot):
    bot.add_cog(Manage(bot))
